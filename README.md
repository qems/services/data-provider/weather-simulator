# weather simulator

This weather simulator provides current weather values and forecasts for those based on data stored in an InfluxDB.
It simulates the forecast realistically by taking only the forecast available in the database, which is the most recent
one according to the current simulation time step.
Thus it enables simulation of rolling horizon systems.
Input data can be collected via the qems-owm-dwd-weather-scraper, which stores the data in the already correct format.

The simulator provides access to the weather state and forecasts via the unified qemsAPI-ontology.

## Mosaik

An instantiation of this simulator in a mosaik simulation is demonstrated below:

```yaml
sim_config:
  Weather:
    python: weather_simulator:WeatherSimulator
simulations:
  weatherSim:
    sim_name: Weather
    sim_start: ${START_DATE_TIME_SIMULATION} # iso timestamp
    forecast_horizon: "72h"
devices:
  weatherA:
    sim: weatherSim.Weather
    influx_url: ${INFLUXDB_URL} # URL to reach your influxdb at
    influx_token: ${INFLUXDB_TOKEN} # Token with access to the bucket mentioned below
    influx_org: ${INFLUXDB_ORG} # org the bucket is located in
    influx_bucket: ${SIMULATION_DATA_INFLUX_BUCKET} # bucket with the simulation data
    latitude: 0.0 # used in location lookups, will use nearest datapoints
    longitude: 0.0
  weatherAProxy:
    sim: schedulerSim.Weather
    dataProvider: "simulator"
    airTemperatureK:
      Now: 0.0
      Prediction:
    soilTemperatureK: # currently this is equal to the airTemperature
      Now: 0.0
      Prediction:
    pressurePa:
      Now: 0.0
      Prediction:
    relativeHumidity:
      Now: 0.0
      Prediction:
    cloudCover:
      Now: 0.0
      Prediction:
    windSpeedMpS:
      Now: 0.0
      Prediction:
    windDirectionD:
      Now: 0.0
      Prediction:
    windGustMpS:
      Now: 0.0
      Prediction:
    globalHorizontalIrradianceWhoM2: {} # not supported by this simulator yet
connections:
  - source: weatherA#/.*airTemperatureK-\d\(Prediction\)/
    sink: <forecast_consuming_simulator>
    attributes:
      - - forecast
        - <input_attribute>
  - source: weatherA#/.*airTemperatureK-\d\(Now\)/
    sink: <current_temperature_consuming_simulator>
    attributes:
      - value
```

Every Weather Model instance contains child entities which represent the separate fields capabilities and contain the
actual attributes.
The names of these child entities follow this pattern:
`"Weather-<weather_model_uuid>.<field_name>-<capability_count>(<capability_name>)"`

So for example the airTemperatureK Now value would have a name like
`Weather-6ab07507-0cf0-489c-8c69-ffeb97e778fa.airTemperatureK-2(Now)`.

The above description of a simulator instantiation shows that every field has a "Read.Now" and "Read.Prediction"
capability.
While the Now-values are just plain numbers, the Prediction child entities contain a forecast attribute, which in turn
contains a dict with unix-timestamps (in milliseconds) as keys and numbers as values.

## InfluxDB schema

This weather simulator expects a specific schema with which the current values and forecasts are stored.

First of all, for the current weather values, the measurement is named `weather_service_current`.
The weather forecast values are stored under the `weather_service_forecast` measurement, with an additional `timeOfPrediction`
tag containing a unix timestamp of when the prediction was made.

All measurements, regardless of current or forecast, are tagged with the following tags:

| Name            | Value                                                                                  |
|-----------------|----------------------------------------------------------------------------------------|
| data_provider   | empty or `owm`                                                                         |
| lat             | latitude the data was recorded at                                                      |
| lon             | longitude the data was recorded at                                                     |
| _field          | `temperature`, `wind_speed`, `clouds`, `pressure`, `humidity`, `wind_deg`, `wind_gust` |
