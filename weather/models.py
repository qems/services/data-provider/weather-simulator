from dataclasses import dataclass
import dataclasses
import uuid


@dataclass
class Capability:
    pass


@dataclass
class Read(Capability):
    pass


@dataclass
class Now(Read):
    value: float

    def __init__(self):
        self.value = 0.0

    def get_data(self) -> dict:
        return {'value': self.value}


@dataclass
class Prediction(Read):
    forecast: dict[int, float]

    def __init__(self):
        self.forecast = {}

    def get_data(self) -> dict:
        return {'forecast': self.forecast}


@dataclass
class Historic(Read):
    history: dict[int, float]

    def __init__(self):
        self.history = {}

    def get_data(self) -> dict:
        return {'history': self.history}


@dataclass
class Entity:
    eid: str

    def _get_child_id(self, attribute_name: str, child_type: str):
        value = getattr(self, attribute_name)
        return f"{self.eid}.{attribute_name}-{str(len(value))}({child_type})"

    def as_mosaik_meta_dict(self) -> dict:
        children = []

        for field in dataclasses.fields(self):
            name = field.name
            value = getattr(self, name)
            if isinstance(value, list):
                for cap in value:
                    type = cap.__class__.__name__
                    children.append({
                        'eid': self._get_child_id(name, type),
                        'type': type
                    })

        return {'eid': self.eid, 'type': self.__class__.__name__, 'children': children}
    
    def get_data(self) -> dict:
        data = {}

        for field in dataclasses.fields(self):
            name = field.name
            value = getattr(self, name)

            if isinstance(value, list):
                # set of capabilities -> children
                for cap in value:
                    type = cap.__class__.__name__
                    data[self._get_child_id(name, type)] = cap.get_data()
            else:
                # "normal" attribute
                new_data = {name: value}
                if self.eid not in data:
                    data[self.eid] = {}
                data[self.eid].update(new_data)

        return data


@dataclass
class Weather(Entity):
    airTemperatureK: list[Capability]
    soilTemperatureK: list[Capability]
    pressurePa: list[Capability]
    relativeHumidity: list[Capability]
    cloudCover: list[Capability]
    windSpeedMpS: list[Capability]
    windDirectionD: list[Capability]
    windGustMpS: list[Capability]
    globalHorizontalIrradianceWhoM2: list[Capability]

    def __init__(self, latitude: float, longitude: float):
        # TODO: Check which data is available in influxdb an instantiate capabilities based on that???
        self.eid = "Weather-" + str(uuid.uuid4())
        self.latitude = latitude
        self.longitude = longitude
        for field in dataclasses.fields(self):
            name = field.name
            if not name in ["airTemperatureK", "soilTemperatureK", "pressurePa", "relativeHumidity", "cloudCover", "windSpeedMpS", "windDirectionD", "windGustMpS", "globalHorizontalIrradianceWhoM2"]:
                # Only these are supported by the current simulator
                continue
            setattr(self, name, [])
            getattr(self, name).append(Now())
            getattr(self, name).append(Prediction())
