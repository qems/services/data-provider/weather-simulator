import collections

import mosaik_api_v3 as mosaik_api
from influxdb_client import InfluxDBClient
import datetime
import arrow
from dataclasses import dataclass

from . import models

import pandas as pd

META = {
    'type': 'time-based',
    'models': {
        'Weather': {
            'public': True,
            'any_inputs': False,
            'params': ['influx_url', 'influx_token', 'influx_org', 'influx_bucket', 'latitude', 'longitude'],
            'attrs': [],
        },
        'Now': {
            'public': False,
            'any_inputs': False,
            'params': [],
            'attrs': ['id', 'value']
        },
        'Prediction': {
            'public': False,
            'any_inputs': False,
            'params': [],
            'attrs': ['id', 'forecast']
        }
    },
}


class WeatherSimulator(mosaik_api.Simulator):

    def __init__(self):
        super().__init__(META)
        self.data = collections.defaultdict(lambda:
                                            collections.defaultdict(list))
        self.step_size = None

        self.influx_connection = None
        self.influx_bucket = None
        self.time_resolution = 1.0
        self.sim_start = None
        self.forecast_horizon = None

        self.weather_models = {}

    def init(self, sid, time_resolution=1.0, sim_start=0, date_format=None, forecast_horizon="24h"):
        self.sim_start = self.get_time(sim_start, date_format)
        self.time_resolution = time_resolution
        self.forecast_horizon = forecast_horizon
        return self.meta

    def create(self, num, model, **params):
        if model == "Weather":
            influx_connection = InfluxDBClient(url=params["influx_url"], token=params["influx_token"], org=params["influx_org"])
            influx_bucket = params["influx_bucket"]

            return_value = []
            for i in range(num):
                instance = models.Weather(latitude=params['latitude'], longitude=params['longitude'])
                instance.influx_connection = influx_connection
                instance.influx_bucket = influx_bucket

                return_value.append(instance.as_mosaik_meta_dict())
                self.weather_models[instance.eid] = instance

            return return_value
        return None
    
    def step(self, time, inputs, max_advance):
        now = self.sim_start.shift(seconds=time * self.time_resolution).datetime

        for k in self.weather_models:
            model = self.weather_models[k]
            df = self.get_weather_forecast(now - datetime.timedelta(hours=1), now + pd.to_timedelta(self.forecast_horizon), now, model)
            timestamp_index = df.index.to_series().apply(lambda dt: int(dt.timestamp()*1000))
            df = df.set_axis(timestamp_index, axis=0)
            now_df = self.get_weather_now(now, model)

            model.airTemperatureK[1].forecast = df['temp_air'].to_dict()
            model.airTemperatureK[0].value = now_df['temp_air'].iloc[0]
            
            model.soilTemperatureK[1].forecast = df['temp_air'].to_dict()
            model.soilTemperatureK[0].value = now_df['temp_air'].iloc[0]

            model.pressurePa[1].forecast = (df['pressure'] / 100).to_dict()
            model.pressurePa[0].value = now_df['pressure'].iloc[0] / 100

            model.relativeHumidity[1].forecast = df['humidity'].to_dict()
            model.relativeHumidity[0].value = now_df['humidity'].iloc[0]

            model.cloudCover[1].forecast = df['clouds'].to_dict()
            model.cloudCover[0].value = now_df['clouds'].iloc[0]
            
            model.windSpeedMpS[1].forecast = df['wind_speed'].to_dict()
            model.windSpeedMpS[0].value = now_df['wind_speed'].iloc[0]

            model.windGustMpS[1].forecast = df['wind_gust'].to_dict()
            model.windGustMpS[0].value = now_df['wind_gust'].iloc[0]

            model.windDirectionD[1].forecast = df['wind_deg'].to_dict()
            model.windDirectionD[0].value = now_df['wind_deg'].iloc[0]

        one_hour_in_timesteps = self.time_resolution * 60 * 60
        return time + int(one_hour_in_timesteps)
    
    def get_weather_now(self, now: datetime.datetime, model: models.Weather):
        query_api = model.influx_connection.query_api()

        params = {
            "_bucket": model.influx_bucket,
            "_measurement": "weather_service_current",
            "_columns": ["temperature", "_time", "wind_speed", "clouds", "pressure", "humidity", "wind_deg", "wind_gust"],
            "_data_provider": "owm",
            "_lat": model.latitude,
            "_lon": model.longitude,
            "_now": now
        }
        # TODO: This query could be easier I think
        data_frame = query_api.query_data_frame(
            'import "experimental/geo"\n'
            'import "math"\n'
            'import "date"\n'
            'from(bucket: _bucket)'
            '   |> range(start: date.sub(d: 1h, from: _now), stop: _now)'
            '   |> filter(fn: (r) => r["_measurement"] == _measurement)'
            '   |> map(fn: (r) => ({ r with lat: float(v: r["lat"]), lon: float(v: r["lon"]) }))'
            '   |> map(fn: (r) => ({ r with delta: geo.ST_Distance(geometry: {lat: r.lat, lon: r.lon}, region: {lat: _lat, lon: _lon}) }))'
            '   |> drop(columns: ["lat", "lon"])'
            '   |> sort(columns: ["delta", "_time"], desc: false)'
            '   |> unique(column: "_time")'
            '   |> sort(columns: ["_time"], desc: false)'
            '   |> last()'
            '   |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")'
            '   |> keep(columns: _columns)',
            params=params)
        data_frame = data_frame.rename(columns={"temperature": "temp_air", "_time": "time"})
        data_frame = data_frame.drop(columns=["result", "table"], errors="ignore")
        if data_frame.size == 0:
            data_frame = pd.DataFrame({'time': [], 'temp_air': [], 'clouds': [], "pressure": [], "wind_speed": [], "humidity": [], "wind_deg": [], "wind_gust": []})
        data_frame.index = pd.to_datetime(data_frame["time"], utc=True)
        data_frame = data_frame.drop(columns=["time"])
        return data_frame

    def get_weather_forecast(self, start: datetime.datetime, stop: datetime.datetime, now: datetime.datetime, model: models.Weather):
        query_api = model.influx_connection.query_api()

        params = {
            "_bucket": model.influx_bucket,
            "_from_day": start,
            "_to_day": stop,
            "_measurement": "weather_service_forecast",
            "_columns": ["temperature", "_time", "wind_speed", "clouds", "pressure", "humidity", "wind_deg", "wind_gust"],
            "_data_provider": "owm",
            "_lat": model.latitude,
            "_lon": model.longitude,
            "_now_timestamp": now.timestamp()
        }
        # TODO: This query could be easier I think
        data_frame = query_api.query_data_frame(
            'import "experimental/geo"\n'
            'import "math"\n'
            'import "date"\n'
            'lastTimeOfPrediction = from(bucket: _bucket)'
            '  |> range(start: date.sub(d: 2h, from: _from_day), stop: date.add(d: 1m, to: _from_day))'
            '  |> filter(fn: (r) => r["_measurement"] == _measurement and int(v: r["timeOfPrediction"]) <= _now_timestamp and (not exists r["data_provider"] or r["data_provider"] == _data_provider))'
            '  |> group()'
            '  |> sort(columns: ["timeOfPrediction"], desc: false)'
            '  |> last(column: "timeOfPrediction")'
            '  |> findRecord(fn: (key) => true, idx: 0)'
            'from(bucket: _bucket)'
            '   |> range(start: _from_day, stop: _to_day)'
            '   |> filter(fn: (r) => r["_measurement"] == _measurement and r["timeOfPrediction"] == lastTimeOfPrediction.timeOfPrediction and (not exists r["data_provider"] or r["data_provider"] == _data_provider))'
            '   |> map(fn: (r) => ({ r with lat: float(v: r["lat"]), lon: float(v: r["lon"]) }))'
            '   |> map(fn: (r) => ({ r with delta: geo.ST_Distance(geometry: {lat: r.lat, lon: r.lon}, region: {lat: _lat, lon: _lon}) }))'
            '   |> drop(columns: ["lat", "lon"])'
            '   |> sort(columns: ["delta", "_time"], desc: false)'
            '   |> unique(column: "_time")'
            '   |> sort(columns: ["_time"], desc: false)'
            '   |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")'
            '   |> keep(columns: _columns)',
            params=params)
        data_frame = data_frame.rename(columns={"temperature": "temp_air", "_time": "time"})
        data_frame = data_frame.drop(columns=["result", "table"], errors="ignore")
        if data_frame.size == 0:
            data_frame = pd.DataFrame({'time': [], 'temp_air': [], 'clouds': [], "pressure": [], "wind_speed": [], "humidity": [], "wind_deg": [], "wind_gust": []})
        data_frame.index = pd.to_datetime(data_frame["time"], utc=True)
        data_frame = data_frame.drop(columns=["time"])
        return data_frame
    
    def get_data(self, outputs):
        data = {}
        for k in self.weather_models:
            model = self.weather_models[k]
            data.update(model.get_data()) # just output everything, what do I care?

        return data

    def get_time(self, date, date_format):
        if date_format is not None:
            return arrow.get(date, self.date_format)
        else:
            return arrow.get(date)

if __name__ == '__main__':
    mosaik_api.start_simulation(WeatherSimulator())
